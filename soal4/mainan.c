#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

int main(int argc, char *argv[]) {
  int hour, minute, second;
  char *path;
  char cmd[1000];
  // Parse command-line arguments
  if (argc != 5) {
      fprintf(stderr, "Error: Invalid number of arguments.\n");
      exit(1);
  }
  if (strcmp(argv[1], "*") == 0) {
      hour = -1;
  } else {
      hour = atoi(argv[1]);
      if (hour < 0 || hour > 23) {
          fprintf(stderr, "Error: Invalid hour argument.\n");
          exit(1);
      }
  }
  if (strcmp(argv[2], "*") == 0) {
      minute = -1;
  } else {
      minute = atoi(argv[2]);
      if (minute < 0 || minute > 59) {
          fprintf(stderr, "Error: Invalid minute argument.\n");
          exit(1);
      }
  }
  if (strcmp(argv[3], "*") == 0) {
      second = -1;
  } else {
      second = atoi(argv[3]);
      if (second < 0 || second > 59) {
          fprintf(stderr, "Error: Invalid second argument.\n");
          exit(1);
      }
  }
  path = argv[4];

  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/syukra/sisop/Modul2/soal4/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    pid_t id,id1;
    id=fork();
    if(id<0)exit(EXIT_FAILURE);
    if(id==0){  
    
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    int curr_hour = tm->tm_hour;
    int curr_minute = tm->tm_min;
    int curr_second = tm->tm_sec;

    if ((hour == -1 || hour == curr_hour) &&
        (minute == -1 || minute == curr_minute) &&
        (second == -1 || second == curr_second)) {
        char *cmd[] = {"bash",path};
        execv("/bin/bash",cmd);
      }
    exit(0);
    }
    sleep(1);
  }
}
