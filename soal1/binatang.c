#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>

void downloadURL(char *url){
    // Download file dari URL
    pid_t pidURL = fork();
    if (pidURL == 0) {
        execlp("wget", "wget", "--no-check-certificate", url, "-O", "hewan.zip", NULL);
        exit(0);
    }

    int statusURL;
    waitpid(pidURL, &statusURL, 0);
}

void unzipDirectory(char *dir){
    pid_t pidUNZIP = fork();
    if (pidUNZIP == 0) {
        execlp("unzip", "unzip", "-q", dir, NULL);
        exit(0);
    }

    int statusUNZIP;
    waitpid(pidUNZIP, &statusUNZIP, 0);
}

void makeDirectory() {
    pid_t pidMAKEDIR = fork();
    if (pidMAKEDIR == 0) {
        execlp("mkdir", "mkdir", "-p", "hewanDarat", "hewanAir", "hewanAmphibi", NULL);
        exit(0);
    }

    int statusMAKEDIR;
    waitpid(pidMAKEDIR, &statusMAKEDIR, 0);
}

void randomShift(){
    system("echo Grapekun akan menjaga hewan: ");
    system("shuf -en 1 *.jpg | cut -d. -f1");
}

void classification(char *dir){
    pid_t pidCLASS;
    int statusCLASS;
    struct dirent *dp;
    DIR *direktori;
    direktori = opendir(dir);

    if (direktori == NULL) {
        perror("Failed to open directory");
        exit(EXIT_FAILURE);
    }

    while ((dp = readdir(direktori)) != NULL){
        if (dp->d_type == DT_REG) {
            int len = strlen(dp->d_name);
            char* extension = dp->d_name + len - 4;

            if (strcmp(extension, ".jpg") == 0) {
                if (strstr(dp->d_name, "darat") != NULL) {
                    if ((pidCLASS = fork()) == 0){
                        char mv_cmd[256];
                        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanDarat/", dir, dp->d_name);
                        system(mv_cmd);
                        exit(EXIT_SUCCESS);
                    }
                    while ((wait(&statusCLASS)) > 0);
                }

                if (strstr(dp->d_name, "air") != NULL) {
                    if ((pidCLASS = fork()) == 0) {
                        char mv_cmd[256];
                        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanAir/", dir, dp->d_name);
                        system(mv_cmd);
                        exit(EXIT_SUCCESS);
                    }
                    while ((wait(&statusCLASS)) > 0);
                }

                if (strstr(dp->d_name, "amphibi") != NULL) {
                    if ((pidCLASS = fork()) == 0) {
                        char mv_cmd[256];
                        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanAmphibi/", dir, dp->d_name);
                        system(mv_cmd);
                        exit(EXIT_SUCCESS);
                    }
                    while ((wait(&statusCLASS)) > 0);
                }
            }
        }
    }

    closedir(direktori);
    waitpid(pidCLASS, &statusCLASS, 0);
    // system("mv *darat* hewanDarat/");
    // system("mv *air* hewanAir/");
    // system("mv *amphibi* hewanAmphibi/");
}

// void classification(char *dir, char *dir_1, char *dir_2, char *dir_3) {
//     pid_t pidCLASS;
//     int statusCLASS;
//     struct dirent *dp;
//     DIR *direktori;
//     direktori = opendir(dir);

//     if (direktori == NULL) {
//         perror("Failed to open directory");
//         exit(EXIT_FAILURE);
//     }

//     while ((dp = readdir(direktori)) != NULL){
//         if (dp->d_type == DT_REG) {
//             int len = strlen(dp->d_name);
//             char* extension = dp->d_name + len - 5;

//             if (strcmp(extension, "darat") == 0) {
//                 if ((pidCLASS = fork()) == 0){
//                     char mv_cmd[256];
//                     snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s %s", dir, dp->d_name, dir_1);
//                     system(mv_cmd);
//                     exit(EXIT_FAILURE);
//                 }
//                 while ((wait(&statusCLASS)) > 0);
//             }

//             if (strcmp(extension, "air") == 0) {
//                 if ((pidCLASS = fork()) == 0) {
//                     char mv_cmd[256];
//                     snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s %s", dir, dp->d_name, dir_2);
//                     system(mv_cmd);
//                     exit(EXIT_FAILURE);
//                 }
//                 while ((wait(&statusCLASS)) > 0);
//             }

//             if (strcmp(extension, "amphibi") == 0) {
//                 if ((pidCLASS = fork()) == 0) {
//                     char mv_cmd[256];
//                     snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s %s", dir, dp->d_name, dir_3);
//                     system(mv_cmd);
//                     exit(EXIT_FAILURE);
//                 }
//                 while ((wait(&statusCLASS)) > 0);
//             }
//         }
//     }

//     closedir(direktori);
//     waitpid(pidCLASS, &statusCLASS, 0);

// }

void zipDirectory() {
  char* zipNames[3] = {"hewanDarat.zip", "hewanAir.zip", "hewanAmphibi.zip"};
  char* directoryNames[3] = {"hewanDarat", "hewanAir", "hewanAmphibi"};

  for (int i = 0; i < 3; i++) {
    pid_t pidZIP = fork();
    if (pidZIP == 0) {
      execl("/usr/bin/zip", "zip", "-r", zipNames[i], directoryNames[i], NULL);
    } else {
      waitpid(pidZIP, NULL, 0);
    }
  }
}

void removeDirectories() {
    system("rm -r hewanDarat/");
    system("rm -r hewanAmphibi/");
    system("rm -r hewanAir/");
}

int main() {
  downloadURL("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq");
  unzipDirectory("hewan.zip");
  randomShift();
  makeDirectory();
  classification(".");
  zipDirectory();
  removeDirectories();
}
