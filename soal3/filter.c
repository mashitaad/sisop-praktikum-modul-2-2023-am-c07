#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>

void downloadURL(char *url){
    // Download file dari URL
    pid_t pidURL = fork();
    if (pidURL == 0) {
        execlp("wget", "wget", "--no-check-certificate", url, "-O", "players.zip", NULL);
        exit(0);
    }

    int statusURL;
    waitpid(pidURL, &statusURL, 0);
}

void unzipDirectory(char *dir){
    pid_t pidUNZIP = fork();
    if (pidUNZIP == 0) {
        execlp("unzip", "unzip", "-q", dir, NULL);
        exit(0);
    }
    int statusUNZIP;
    waitpid(pidUNZIP, &statusUNZIP, 0);

    if (WIFEXITED(statusUNZIP) && !WEXITSTATUS(statusUNZIP)) {
        // unzip success, delete file.zip
        pid_t pidRM = fork();
        if (pidRM == 0) {
            execlp("rm", "rm", dir, NULL);
            exit(0);
        }
        int statusRM;
        waitpid(pidRM, &statusRM, 0);
    }
}


void removePlayer(char *directory) {
  DIR *folder;
  struct dirent *entry;

  folder = opendir(directory);

  while ((entry = readdir(folder)) != NULL) {
    if (entry->d_type == DT_DIR) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
        continue;

      char subdirectory[strlen(directory) + strlen(entry->d_name) + 2];
      sprintf(subdirectory, "%s/%s", directory, entry->d_name);
      pid_t pidRP = fork();

      if (pidRP == 0) {
        removePlayer(subdirectory);
        exit(EXIT_SUCCESS);
      } else {
        int stat;
        waitpid(pidRP, &stat, 0);
      }
    } else if (entry->d_type == DT_REG) {
      char *extension = strrchr(entry->d_name, '.');
      if (extension && strcmp(extension, ".png") == 0) {
        char *search_result = strstr(entry->d_name, "ManUtd");
        if (search_result == NULL) {
          char file_path[strlen(directory) + strlen(entry->d_name) + 2];
          sprintf(file_path, "%s/%s", directory, entry->d_name);
          remove(file_path);
        }
      }
    }
  }

  closedir(folder);
}

void makeDirectory() {
    pid_t pidMAKEDIR = fork();
    if (pidMAKEDIR == 0) {
        execlp("mkdir", "mkdir", "-p", "players/Kiper", "players/Bek", "players/Gelandang", "players/Penyerang", NULL);
        exit(0);
    }

    int statusMAKEDIR;
    waitpid(pidMAKEDIR, &statusMAKEDIR, 0);
}


void categorizePlayer(char *directory) {

  DIR *folder;
  struct dirent *entry;

  folder = opendir(directory);

  while ((entry = readdir(folder)) != NULL) {
    if (entry->d_type == DT_REG) {
      char *extension = strrchr(entry->d_name, '.');
      if (extension && strcmp(extension, ".png") == 0) {
        char *search_result = strstr(entry->d_name, "ManUtd");
        if (search_result != NULL) {
          char file_path[strlen(directory) + strlen(entry->d_name) + 2];
          sprintf(file_path, "%s/%s", directory, entry->d_name);

          char *posisi = NULL;

          // Cek posisi pemain berdasarkan nama file
          if (strstr(entry->d_name, "Kiper")) {
            posisi = "Kiper";
          } else if (strstr(entry->d_name, "Bek")) {
            posisi = "Bek";
          } else if (strstr(entry->d_name, "Gelandang")) {
            posisi = "Gelandang";
          } else if (strstr(entry->d_name, "Penyerang")) {
            posisi = "Penyerang";
          }

          // Pindahkan file ke folder yang sesuai
          if (posisi != NULL) {
            char new_path[strlen(posisi) + strlen(entry->d_name) + strlen(directory) + 3];
            sprintf(new_path, "%s/%s/%s", directory, posisi, entry->d_name);

            pid_t pidMV = fork();
            if (pidMV == 0) {
              execlp("mv", "mv", file_path, new_path, NULL);
              exit(0);
            } else {
              int statusMV;
              waitpid(pidMV, &statusMV, 0);
            }
          }
        }
      }
    }
  }

  closedir(folder);
}

void makeTeam(int bek, int gelandang, int penyerang) {
    char* posisi[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};
    int jumlah_pemain[] = {1, bek, gelandang, penyerang};


    // Membuka file output di direktori players
    char output_file_path[100];
    sprintf(output_file_path, "players/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *output_file = fopen(output_file_path, "w");

    // Menulis header file output
    fprintf(output_file, "Formasi %d-%d-%d\n", bek, gelandang, penyerang);
    fprintf(output_file, "-----------------\n");

    // Mencari file pemain di setiap posisi
    for (int i = 0; i < 4; i++) {
        DIR *folder;
        struct dirent *entry;

        char directory[100];
        sprintf(directory, "players/%s", posisi[i]);

        folder = opendir(directory);

        // Mencari pemain dengan rating tertinggi di setiap posisi
        int count = 0;
        while ((entry = readdir(folder)) != NULL && count < jumlah_pemain[i]) {
            if (entry->d_type == DT_REG) {
                char file_path[strlen(directory) + strlen(entry->d_name) + 2];
                sprintf(file_path, "%s/%s", directory, entry->d_name);

                // Membaca rating pemain dari nama file
                int rating = atoi(strtok(entry->d_name, "_"));

                // Menuliskan informasi pemain ke file output
                fprintf(output_file, "%s %d %s\n", posisi[i], rating, entry->d_name);

                count++;
            }
        }

        closedir(folder);
    }

    fclose(output_file);
}

int main() {
  downloadURL("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF");
  unzipDirectory("players.zip");
  removePlayer("players");
  makeDirectory();
  categorizePlayer("players");
  makeTeam(4, 3, 3);
  return 0;
}
