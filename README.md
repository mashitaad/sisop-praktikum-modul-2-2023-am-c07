# sisop-latihan-modul-2-2023-AM-C07

## Identitas Kelompok
| Name            | NRP        |
| ---             | ---        |
| Mashita Dewi    | 5025211036 |
| Syukra Wahyu R  | 5025211037 |
| Akhmad Mustofa S| 5025211230 | 

## Soal 1
1. Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
    - Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
    - Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
    - Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
    - Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system

## Penyelesaian
```ruby
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
```
Baris ini adalah header file yang digunakan dalam program, yang mengandung fungsi-fungsi standar dan tipe data yang dibutuhkan.
- `include <stdio.h>` Header file ini mengandung fungsi standar input-output seperti printf, scanf, dan sprintf
- `include <stdlib.h>` Header file ini mengandung fungsi standar untuk memanipulasi memori dan melakukan konversi tipe data seperti malloc, free, dan atoi
- `include <unistd.h>` Header file ini mengandung fungsi standar untuk sistem operasi seperti system dan mkdir
- `include <time.h>` Header time.h mengandung fungsi-fungsi yang digunakan untuk memanipulasi waktu dan tanggal. Beberapa fungsi yang tersedia di antaranya adalah time, localtime, gmtime, mktime, strftime, dan lain-lain. Header ini sangat berguna dalam banyak aplikasi seperti pemrograman jadwal, pengaturan waktu dalam program, dan logging.
- `include <sys/types.h>` Header sys/types.h berisi definisi tipe data yang digunakan dalam sistem operasi Unix. Beberapa tipe data yang didefinisikan dalam header ini adalah pid_t, off_t, size_t, time_t, dan lain-lain. Header ini sering digunakan bersamaan dengan header unistd.h dan fcntl.h.
- `include <string.h>` Header string.h berisi deklarasi fungsi-fungsi manipulasi string dalam bahasa C, seperti strlen, strcpy, strcat, strcmp, dan lain-lain. Header ini sangat berguna dalam banyak aplikasi, terutama dalam pemrograman teks dan parsing.
- `include <sys/stat.h>` Header sys/stat.h berisi definisi fungsi-fungsi yang digunakan untuk mengambil informasi tentang file, seperti ukuran file, waktu modifikasi, hak akses, dan lain-lain. Header ini berguna dalam banyak aplikasi yang memanipulasi file dan direktori di sistem operasi Unix. Beberapa fungsi yang tersedia di antaranya adalah stat, lstat, fstat, chmod, chown, utime, dan lain-lain.

```ruby
void downloadURL(char *url){
    // Download file dari URL
    pid_t pidURL = fork();
    if (pidURL == 0) {
        execlp("wget", "wget", "--no-check-certificate", url, "-O", "hewan.zip", NULL);
        exit(0);
    }

    int statusURL;
    waitpid(pidURL, &statusURL, 0);
}
```
- `void downloadURL(char *url){`Mendeklarasikan fungsi bernama downloadURL yang menerima satu parameter berupa URL yang akan digunakan untuk mengunduh sebuah drive yang telah ditentukan.
- `pid_t pid1 = fork()` Membuat sebuah child process dengan menggunakan fungsi `fork()` dan menyimpan nilai PID (Process ID) dari child process tersebut ke dalam variabel `pidURL`.
- `if (pid1 == 0) {` Melakukan pengecekan apakah nilai `pidURL` sama dengan 0. Jika ya, berarti saat ini sedang berada di dalam child process.
- `execlp("wget", "wget", "--no-check-certificate", url, "-O", "hewan.zip", NULL)` ika sedang berada di dalam child process, maka akan menjalankan perintah wget dengan menggunakan fungsi `execlp()`. Argumen-argumen yang diberikan kepada execlp() adalah nama program yang akan dijalankan (wget) serta argumen-argumen yang akan diberikan kepada program tersebut `(--no-check-certificate, url, -O, dan hewan.zip)`. Argumen terakhir yaitu NULL digunakan untuk menandakan akhir dari argumen-argumen yang diberikan.
- `exit(0)` Mengakhiri proses child.
- `int statusURL` Membuat variabel statusURL yang akan digunakan untuk menyimpan status dari child process.
- `waitpid(pidURL, &statusURL, 0)`  Menunggu child process dengan PID pidURL selesai dijalankan dengan menggunakan fungsi waitpid(). Status dari child process akan disimpan di dalam variabel `statusURL`.

```ruby
void unzipDirectory(char *dir){
    pid_t pidUNZIP = fork();
    if (pidUNZIP == 0) {
        execlp("unzip", "unzip", "-q", dir, NULL);
        exit(0);
    }

    int statusUNZIP;
    waitpid(pidUNZIP, &statusUNZIP, 0);
}
```
- `void unzipDirectory(char *dir){` Mendefinisikan fungsi unzipDirectory dengan parameter dir yang merupakan alamat direktori yang akan di-unzip.
- `pid_t pidUNZIP = fork()` Membuat proses baru dengan memanggil fungsi `fork()`, dan menyimpan nilai pid (Process ID) dari proses tersebut ke variabel pidUNZIP.
- `if (pidUNZIP == 0) {` Memeriksa apakah nilai dari pidUNZIP adalah 0, yang menandakan bahwa proses saat ini berada di dalam child process.
- `execlp("unzip", "unzip", "-q", dir, NULL)` Jika proses saat ini berada di dalam child process, maka memanggil `execlp()` untuk menjalankan perintah unzip dengan argumen `-q` (quiet mode) dan dir sebagai alamat direktori yang akan di-unzip.
- `exit(0)` Keluar dari child process dengan status keluaran 0 (sukses).
- `int statusUNZIP` Mendeklarasikan variabel statusUNZIP yang akan digunakan untuk menyimpan status keluaran dari child process.
- `waitpid(pidUNZIP, &statusUNZIP, 0)` Menunggu child process yang memiliki pid `pidUNZIP` untuk selesai, dan menyimpan status keluaran dari child process ke variabel `statusUNZIP`.

```ruby
void makeDirectory() {
    pid_t pidMAKEDIR = fork();
    if (pidMAKEDIR == 0) {
        execlp("mkdir", "mkdir", "-p", "hewanDarat", "hewanAir", "hewanAmphibi", NULL);
        exit(0);
    }

    int statusMAKEDIR;
    waitpid(pidMAKEDIR, &statusMAKEDIR, 0);
}
```
- `void makeDirectory() {` Mendefinisikan fungsi makeDirectory dengan tipe data void dan tidak memiliki parameter.
- `pid_t pidMAKEDIR = fork()` Membuat sebuah child process dengan menggunakan fungsi fork().Mengembalikan nilai 0 untuk child process dan PID (Process ID) dari child process untuk parent process.
- `if (pidMAKEDIR == 0) {` Menjalankan kode di dalam blok if hanya jika nilai dari pidMAKEDIR sama dengan 0, yang menandakan bahwa ini adalah child process. 
- `execlp("mkdir", "mkdir", "-p", "hewanDarat", "hewanAir", "hewanAmphibi", NULL)` Proses anak kemudian menjalankan perintah `execlp()` dengan argumen "mkdir", "mkdir", "-p", "hewanDarat", "hewanAir", dan "hewanAmphibi". Fungsi mkdir digunakan untuk membuat direktori baru `-p` digunakan untuk membuat direktori induk jika belum ada dan membuat direktori anaknya.
- `exit(0)` Mengakhiri proses child.
- `int statusMAKEDIR` Mendeklarasikan variabel status3 yang akan menyimpan status dari proses child.
- `waitpid(pidMAKEDIR, &statusMAKEDIR, 0)` Proses induk menunggu proses anak selesai menggunakan `waitpid()`, dan menyimpan status keluaran dari proses anak ke dalam variabel `statusMAKEDIR`.

```ruby
void randomShift(){
    system("echo Grapekun akan menjaga hewan: ");
    system("shuf -en 1 *.jpg | cut -d. -f1");
}
```
- `void randomShift(){` Mendefinisikan sebuah fungsi bernama "randomShift".
- `system("echo Grapekun akan menjaga hewan: ")` Menampilkan pesan "Grapekun akan menjaga hewan: " di terminal.
- `shuf -en 1 .jpg` Perintah `shuf` digunakan untuk mengacak daftar file yang sesuai dengan pola ".jpg". Sedangkan, opsi `-en 1` digunakan untuk memilih 1 item secara acak dari daftar file yang dihasilkan.
- Menggunakan perintah `cut` untuk memotong nama file hasil acak dan memisahkan bagian sebelum karakter "." (titik), yang merupakan ekstensi file.
- `|` simbol pipa (|) digunakan untuk mengalirkan hasil keluaran dari perintah sebelumnya ke perintah berikutnya.
- `cut -d. -f1` perintah `cut` digunakan untuk memotong atau mengambil bagian tertentu dari setiap baris pada hasil keluaran yang diteruskan dari perintah sebelumnya. Opsi `-d.` digunakan untuk menentukan pemisah (delimiter) pada titik (".") dan `-f1` digunakan untuk memilih bagian pertama sebelum pemisah, yaitu nama file tanpa ekstensi.

```ruby
void classification(char *dir){
    pid_t pidCLASS;
    int statusCLASS;
    struct dirent *dp;
    DIR *direktori;
    direktori = opendir(dir);

    if (direktori == NULL) {
        perror("Failed to open directory");
        exit(EXIT_FAILURE);
    }

    while ((dp = readdir(direktori)) != NULL){
        if (dp->d_type == DT_REG) {
            int len = strlen(dp->d_name);
            char* extension = dp->d_name + len - 4;

            if (strcmp(extension, ".jpg") == 0) {
                if (strstr(dp->d_name, "darat") != NULL) {
                    if ((pidCLASS = fork()) == 0){
                        char mv_cmd[256];
                        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanDarat/", dir, dp->d_name);
                        system(mv_cmd);
                        exit(EXIT_SUCCESS);
                    }
                    while ((wait(&statusCLASS)) > 0);
                }

                if (strstr(dp->d_name, "air") != NULL) {
                    if ((pidCLASS = fork()) == 0) {
                        char mv_cmd[256];
                        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanAir/", dir, dp->d_name);
                        system(mv_cmd);
                        exit(EXIT_SUCCESS);
                    }
                    while ((wait(&statusCLASS)) > 0);
                }

                if (strstr(dp->d_name, "amphibi") != NULL) {
                    if ((pidCLASS = fork()) == 0) {
                        char mv_cmd[256];
                        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanAmphibi/", dir, dp->d_name);
                        system(mv_cmd);
                        exit(EXIT_SUCCESS);
                    }
                    while ((wait(&statusCLASS)) > 0);
                }
            }
        }
    }

    closedir(direktori);
    waitpid(pidCLASS, &statusCLASS, 0);
}
```
Kode di atas merupakan implementasi fungsi classification yang melakukan klasifikasi gambar hewan berdasarkan kata kunci pada nama filenya. Berikut ini adalah penjelasan per baris dari kode tersebut :
```ruby
pid_t pidCLASS;
int statusCLASS;
struct dirent *dp;
DIR *direktori;
direktori = opendir(dir);
```
- `pid_t pidCLASS` variabel untuk menyimpan process id (PID) dari child process yang akan dibuat untuk mengklasifikasikan gambar.
- `int statusCLASS` variabel untuk menyimpan status exit dari child process yang telah selesai dijalankan.
- `struct dirent *dp` struktur yang menyimpan informasi direktori dan file di dalamnya, seperti nama file, ukuran file, tipe file, dan lain-lain.
- `DIR *direktori` variabel yang menyimpan alamat direktori yang akan dijelajahi.

```ruby
if (direktori == NULL) {
    perror("Failed to open directory");
    exit(EXIT_FAILURE);
}
```
- `if (direktori == NULL) {...}` kode untuk mengecek apakah direktori yang akan dibuka berhasil dibuka atau tidak. Jika tidak berhasil dibuka, maka akan muncul pesan error "Failed to open directory" dan program akan berhenti.
- Jika tidak berhasil dibuka, maka akan menampilkan pesan error "Failed to open directory" dengan menggunakan fungsi `perror`
- Kemudian program akan dihentikan menggunakan `exit(EXIT_FAILURE)`.

```ruby
while ((dp = readdir(direktori)) != NULL){
    if (dp->d_type == DT_REG) {
        int len = strlen(dp->d_name);
        char* extension = dp->d_name + len - 4;

        if (strcmp(extension, ".jpg") == 0) {
```
- `while ((dp = readdir(direktori)) != NULL){` Perulangan yang akan dijalankan selama masih terdapat file di dalam direktori.
- `if (dp->d_type == DT_REG` Kode untuk mengecek apakah yang diakses adalah file, bukan direktori atau symlink.
- `int len = strlen(dp->d_name)` Variabel `len` untuk menyimpan panjang dari nama file.
- `char* extension = dp->d_name + len - 4` Variabel extension untuk menyimpan ekstensi dari nama file.
- `if (strcmp(extension, ".jpg") == 0)` Kode untuk mengecek apakah ekstensi dari nama file adalah ".jpg".

```ruby
if (strstr(dp->d_name, "darat") != NULL) {
    if ((pidCLASS = fork()) == 0){
        char mv_cmd[256];
        snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanDarat/", dir, dp->d_name);
        system(mv_cmd);
        exit(EXIT_SUCCESS);
    }
    while ((wait(&statusCLASS)) > 0);
}
```
- `if (strstr(dp->d_name, "darat") != NULL) {` Kode untuk mengecek apakah nama file mengandung kata "darat".
- `if ((pidCLASS = fork()) == 0){` Kode untuk membuat child process dengan menggunakan `fork()`.
- `char mv_cmd[256]` Variabel `mv_cmd` untuk menyimpan perintah mv (move) yang akan dijalankan oleh `system()`.
- `snprintf(mv_cmd, sizeof(mv_cmd), "mv %s/%s hewanDarat/", dir, dp->d_name)` Kode untuk mengisi variabel `mv_cmd` dengan perintah mv yang memindahkan file ke direktori.
- `while ((wait(&statusCLASS)) > 0)` Digunakan untuk menunggu sampai proses child selesai dijalankan dan mengembalikan status keluaran dari proses tersebut ke statusCLASS. Fungsi `wait()` digunakan untuk menunggu hingga sebuah child process selesai dijalankan. Saat fungsi `wait()` dipanggil, ia akan memblokir program utama sampai child process selesai dijalankan. Kemudian, ia akan mengembalikan status keluaran dari child process ke variabel yang ditunjuk oleh pointer yang dilewatkan ke dalamnya (statusCLASS dalam kasus ini). Dalam kasus ini, loop while digunakan untuk menunggu sampai semua child process selesai dijalankan sebelum melanjutkan eksekusi program utama.

```ruby
void zipDirectory() {
  char* zipNames[3] = {"hewanDarat.zip", "hewanAir.zip", "hewanAmphibi.zip"};
  char* directoryNames[3] = {"hewanDarat", "hewanAir", "hewanAmphibi"};

  for (int i = 0; i < 3; i++) {
    pid_t pidZIP = fork();
    if (pidZIP == 0) {
      execl("/usr/bin/zip", "zip", "-r", zipNames[i], directoryNames[i], NULL);
    } else {
      waitpid(pidZIP, NULL, 0);
    }
  }
}
```
- `void zipDirectory() {` Mendefinisikan sebuah fungsi bernama "zipDirectory".
- `char* zipNames[3] = {"hewanDarat.zip", "hewanAir.zip", "hewanAmphibi.zip"}` 
  `char* directoryNames[3] = {"hewanDarat", "hewanAir", "hewanAmphibi"}` Mendefinisikan array string zipNames dan directoryNames yang berisi nama file zip dan nama direktori yang akan di-zip.
- `for (int i = 0; i < 3; i++) {` Melakukan looping sebanyak tiga kali untuk setiap direktori.
- `pid_t pidZIP = fork()` Kode ini membuat child process dengan menggunakan `fork()` dan menyimpan hasilnya di variabel `pidZIP`.
- Jika `pidZIP` sama dengan 0, ini berarti kita berada di child process dan program akan mengeksekusi perintah `execl("/usr/bin/zip", "zip", "-r", zipNames[i], directoryNames[i], NULL)`;\ menggunakan fungsi `execl()`.
- Perintah ini akan men-zip folder yang ditentukan dalam variabel `directoryNames[i]` dan memberikan nama file zip sesuai dengan nilai yang disimpan dalam `zipNames[i]`.
- `waitpid(pidZIP, NULL, 0)` Setelah proses zip selesai, program menunggu proses child (proses zip) selesai dengan menggunakan waitpid. Fungsi waitpid() ini memungkinkan parent process (program utama) menunggu proses child (proses zip) untuk menyelesaikan tugasnya sebelum melanjutkan ke perintah selanjutnya. Pada kode tersebut, waitpid dipanggil dengan argumen NULL dan 0, yang berarti program menunggu proses child mana pun untuk menyelesaikan tugasnya dan menggunakan opsi default.

```ruby
void removeDirectories() {
    system("rm -r hewanDarat/");
    system("rm -r hewanAmphibi/");
    system("rm -r hewanAir/");
}
```
Kode di atas merupakan sebuah fungsi bernama removeDirectories(), yang bertujuan untuk menghapus ketiga direktori yang telah dibuat pada tahap sebelumnya yaitu direktori hewanDarat, hewanAmphibi, dan hewanAir.
- Untuk menghapus sebuah direktori di Linux, kita dapat menggunakan perintah `rm -r`, dimana opsi `-r` digunakan untuk menghapus direktori beserta seluruh isinya secara rekursif.

```ruby
int main() {
  downloadURL("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq");
  unzipDirectory("hewan.zip");
  randomShift();
  makeDirectory();
  classification(".");
  zipDirectory();
  removeDirectories();
}
``` 
Kode tersebut merupakan implementasi dari serangkaian fungsi untuk mengunduh file dari URL tertentu, mengekstrak file zip, melakukan shift acak pada gambar, melakukan klasifikasi gambar, mengompresi direktori menjadi file zip, dan menghapus direktori.
- Pertama, fungsi `downloadURL()` digunakan untuk mengunduh file dari URL tertentu.
- Kemudian, fungsi `unzipDirectory()` digunakan untuk mengekstrak file zip tersebut.
- Fungsi `randomShift()` digunakan untuk memindahkan gambar secara acak dalam direktori yang sama.
- Fungsi `makeDirectory()` digunakan untuk membuat tiga direktori baru dengan nama "hewanDarat", "hewanAir", dan "hewanAmphibi".
- Fungsi `classification()` digunakan untuk mengklasifikasikan gambar ke dalam salah satu direktori sesuai dengan jenis hewan tersebut (darat, air, atau amphibi).
- Fungsi `zipDirectory()` digunakan untuk mengompresi masing-masing direktori menjadi file zip.
- Terakhir, fungsi `removeDirectories()` digunakan untuk menghapus direktori yang telah digunakan sebelumnya.
Semua fungsi tersebut dipanggil secara berurutan dalam fungsi main() untuk melakukan semua tugas yang dijelaskan di atas.

## Soal 2
2. Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !
    - Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
    - Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
    - Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
    - Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
    - Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
Tidak boleh menggunakan system()
Proses berjalan secara daemon
Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

## Penyelesaian
Soal ini mengharuskan kita menggunakan metode daemon, untuk implementasi daemon sendiri seperti Berikut
```Ruby
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

int main() {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    // Tulis program kalian di sini

    sleep(30);
  }
}
```
##### 1.Pembuatan direktori baru
```Ruby
      pid1 =fork();
        if(pid1 < 0)exit(0);
        if(pid1 == 0){ 
          char dirname[20]; // untuk menampung nama direktori
          time_t t = time(NULL); // mendapatkan waktu saat ini
          struct tm tm = *localtime(&t);
          strftime(dirname, sizeof(dirname), "%Y-%m-%d_%H:%M:%S", &tm);//menyimpan waktu pada saat itu ke dirname
          
          if(pid==0){
          char path[1024]; 
          getcwd(path,sizeof(path));//mendapatkan direktori kerja saat ini
          strcat(path,"/");
          strcat(path,dirname);
          pid_t id =fork();
          if(id==0){
                char *arg[] = {"mkdir","-p",dirname,NULL};//menyimpan argumen untuk membuat dirktori baru
                execv("/bin/mkdir", arg);}
          waitpid(id,NULL,0);
```
- Dibutuhkan `fork()` untuk menciptakan child proces baru yang mana di child tsb hal yang dilakukan adalah membuat direktori baru.
    - `char dirname[20];` variabel yang menyimpan nama dari direktori yang akan dibuat
    - `time_t t = time(NULL);struct tm tm = *localtime(&t);` berfungsi untuk mendapatkan waktu pada saat itu
    - `strftime(dirname, sizeof(dirname), "%Y-%m-%d_%H:%M:%S", &tm);`menambahkan waktu pada saat itu dengan Format [YYYY-MM-dd_HH:mm:ss] ke varibel dirname.
    - `getcwd(path,sizeof(path));` berfungsi untuk mendapatkan current work direktori saat ini
    - `strcat(path,dirname);` tujuannya agar direktori baru terbuat di direktori yang sama dengan direktori program berada
   ```ruby
        pid_t id =fork();
        if(id==0){
        char *arg[] = {"mkdir","-p",dirname,NULL};//menyimpan argumen untuk membuat dirktori baru
        execv("/bin/mkdir", arg);}
    waitpid(id,NULL,0);
    ```
- Dilakukan fork() yang mana pada child process hanya menjalankan proces pembuatan direktori baru
    - `waitpid(id,NULL,0);` ini berfungsi untuk menunggu sampai direktori terbuat terlebih dahulu
    
##### 2. Download foto 15 buah, dengan format nama timestamp, dalam direktori baru.
setelah direktori baru terbuat dilakukan download foto sebanyak 15 buah

```Ruby
        for(int i = 0; i <=15; i++){
          id1 = fork();
          if(id1 == 0){
              chdir(path);//mengganti direktori kerja dalam direktori yang baru saja dibuat
              char filename[1024];
              char url[1024];
              t = time(NULL);
              tm = *localtime(&t);
              strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S", &tm);
              snprintf(url,sizeof(url), "https://picsum.photos/%ld", t%1000+50);
              char * args[] = {"wget",url,"-O",filename,"-o","/dev/null",NULL};  // argumen untuk download foto
              execv("/usr/bin/wget", args);
            }
            sleep(5);}// jeda download foto 5 detik
          }
```

- pada proses ini digunakan fork() pada setiap perulangan karena, kita akan menggunakan exec yang mana exec sendiri akan menggantikan process utama
    - `for(int i = 0; i <=15; i++)` perulangan 15x
    - `chdir(path);` mengganti current work direktori ke dalam direktori yang baru dibuat.
    - `strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S", &tm);` menambahkan waktu saat ini ke dalam variabel filename;
    - `snprintf(url,sizeof(url), "https://picsum.photos/%ld", t%1000+50);` menambahkan link foto ke varibel url;
    - `char * args[] = {"wget",url,"-O",filename,"-o","/dev/null",NULL};` declare arg yang berisi argumen penggunaan wget.
    - `sleep(5);` berfungsi untuk menghentikan perulangan selama 5 detik sesuai Soal

#### 3. setelah semua download selesai dilakukan zip pada direktori tsb.
    ```Ruby
    waitpid(id1,NULL,0);//berfungsi untuk menunggu semua download seleseai terlebih dahulu baru zipping dijalankan
    //zipping
	char fileName[30];
    strcat(fileName,dirname);
	snprintf(fileName, sizeof(fileName), "%s.zip", dirname);
	char * arg[] = {"zip", "-rm", fileName, dirname, NULL};
	execv("/usr/bin/zip", arg);
    ```
- `waitpid(id1,NULL,0);` ini berfungsi untuk menunggu child process pertama selesai terlebih dahulu
    - `strcat(fileName,dirname);` menampung nama zip nantinya.
    - `char * arg[] = {"zip", "-rm", fileName, dirname, NULL};` menyimpan argumen untuk zip.

##### 4. Pada soal kita juga diminta untuk membuat killer dari proses ini, killer ini dibuat berdasarkan argumen yang diberikan
  - jika argumen yang diberikan -a maka killer yang dibuat adalah untuk menghentikan semua proses pada saat itu

  ```Ruby
  if(strcmp("-a", argv[1]) == 0){
		fprintf(killer, "killall -SIGKILL lukisan\n");//untuk kill semua process dengan nama test pada saat itu juga}
  ```
  -` killall -SIGKILL lukisan ` merupakan perintah untuk kill semua proses dengan nama lukisan

  - jika argumen yang diberikan adalah -b makan killer yang dibuat adalah untuk menghentikan proses, akan tetapi menunggu semua proses yang sudah berjalan selesai terlebih dahulu, hal ini dapat dilakukan dengan menghentikan parrent proses dari program ini

  ```Ruby
  else if(strcmp("-b", argv[1]) == 0){
	fprintf(killer,"kill -9 %d\n",(int)getpid()+1);//untuk kill parent process saja sehingga child masih dapat menyelesaikan procesnya}
	fprintf(killer, "rm $0\n");
  ```
  -`killer,"kill -9 %d\n",(int)getpid()+1);` merupakan perintah untuk menghentikan parrent proses dari program ini
  
  -`(int)getpid()+1)` berfungsi untuk mendapatkan pid dari parent prosessnya
  
  -`killer, "rm $0\n"` berfungsi untuk menghapus killer setelah dijalankan


## Soal 3
3. Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
    - Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
    - Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
    - Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dengan waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
    - Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Catatan:
Format nama file yang akan diunduh dalam zip berupa [nama]_[tim]_[posisi]_[rating].jpg
Tidak boleh menggunakan system()
Tidak boleh memakai function C mkdir() ataupun rename().
Gunakan exec() dan fork().
Directory “.” dan “..” tidak termasuk yang akan dihapus.
Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

##Penyelesaian
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>
```
- stdio.h: digunakan untuk fungsi input-output standar, seperti printf() dan scanf().
- stdlib.h: digunakan untuk fungsi-fungsi umum, seperti pengalokasian memori dinamis dan konversi tipe data.
- unistd.h: digunakan untuk fungsi-fungsi yang berkaitan dengan sistem operasi Unix, seperti fork() dan exec().
- time.h: digunakan untuk fungsi-fungsi yang berkaitan dengan waktu dan tanggal, seperti time() dan strftime().
- sys/types.h: digunakan untuk mendefinisikan tipe data yang digunakan dalam sistem operasi, seperti pid_t dan size_t.
- dirent.h: digunakan untuk fungsi-fungsi yang berkaitan dengan direktori dan file, seperti opendir() dan readdir().
- string.h: digunakan untuk fungsi-fungsi yang berkaitan dengan manipulasi string, seperti strcpy() dan strlen().
- pwd.h: digunakan untuk fungsi-fungsi yang berkaitan dengan informasi pengguna, seperti getpwuid().
regex.h: digunakan untuk fungsi-fungsi yang berkaitan dengan ekspresi reguler, seperti regcomp() dan regexec().
- sys/stat.h: digunakan untuk fungsi-fungsi yang berkaitan dengan informasi status file, seperti stat() dan chmod().

```
void downloadURL(char *url){
    // Download file dari URL
    pid_t pidURL = fork();
    if (pidURL == 0) {
        execlp("wget", "wget", "--no-check-certificate", url, "-O", "players.zip", NULL);
        exit(0);
    }

    int statusURL;
    waitpid(pidURL, &statusURL, 0);
}
```
- `void downloadURL(char *url){` Mendefinisikan fungsi bernama downloadURL dengan satu parameter bertipe char pointer yang menyimpan URL file yang akan didownload.
- `pid_t pidURL = fork();` Mendefinisikan variabel bertipe pid_t bernama pidURL untuk menyimpan hasil dari pemanggilan sistem fork(). Pemanggilan ini digunakan untuk membuat proses baru (child process).
- `if (pidURL == 0) {` Memeriksa apakah pidURL memiliki nilai 0. Jika iya, maka itu berarti proses yang dijalankan saat ini adalah proses anak.
- `execlp("wget", "wget", "--no-check-certificate", url, "-O", "players.zip", NULL);` Menggunakan sistem execlp() untuk menjalankan perintah wget dengan beberapa opsi dan argumen. Argumen pertama (wget) adalah nama program yang akan dijalankan. Argumen kedua (--no-check-certificate) adalah opsi untuk mendownload file tanpa memeriksa sertifikat SSL. Argumen ketiga (url) adalah URL file yang akan didownload. Argumen keempat (-O) adalah opsi untuk menamai file hasil download dengan nama players.zip. Argumen terakhir (NULL) menunjukkan akhir dari daftar argumen.
- `exit(0)` Keluar dari proses anak.
- `int statusURL` Mendefinisikan variabel bertipe int bernama statusURL untuk menyimpan status dari proses anak yang dijalankan sebelumnya.
- `waitpid(pidURL, &statusURL, 0)` Fungsi waitpid() digunakan untuk menunggu proses anak yang sesuai dengan pidURL selesai dieksekusi. Status dari proses anak tersebut kemudian disimpan ke dalam variabel statusURL.

```
void unzipDirectory(char *dir){
    pid_t pidUNZIP = fork();
    if (pidUNZIP == 0) {
        execlp("unzip", "unzip", "-q", dir, NULL);
        exit(0);
    }
    int statusUNZIP;
    waitpid(pidUNZIP, &statusUNZIP, 0);

    if (WIFEXITED(statusUNZIP) && !WEXITSTATUS(statusUNZIP)) {
        // unzip success, delete file.zip
        pid_t pidRM = fork();
        if (pidRM == 0) {
            execlp("rm", "rm", dir, NULL);
            exit(0);
        }
        int statusRM;
        waitpid(pidRM, &statusRM, 0);
    }
}
```
- `void unzipDirectory(char *dir){` Membuat fungsi bernama unzipDirectory yang menerima argumen berupa string dir.
- `pid_t pidUNZIP = fork()` Membuat variabel bertipe data pid_t dengan nama pidUNZIP yang berfungsi untuk menyimpan ID proses saat melakukan operasi fork().
- `if (pidUNZIP == 0) {` Memeriksa apakah nilai pidUNZIP sama dengan 0. Jika benar, maka kita berada di dalam child process dan akan menjalankan perintah execlp.
- `execlp("unzip", "unzip", "-q", dir, NULL)` Memanggil fungsi execlp dengan parameter "unzip", "unzip", "-q", dir, NULL. Fungsi ini digunakan untuk mengekstrak (unzip) file dengan nama yang sesuai dengan nilai dir ke dalam direktori saat ini dengan opsi -q yang berfungsi untuk mengurangi output yang dihasilkan.
- `exit(0)` Mengakhiri child process
- `int statusUNZIP` Variabel statusUNZIP akan menyimpan status akhir dari proses yang telah dijalankan.
- `waitpid(pidUNZIP, &statusUNZIP, 0)` Memeriksa status dari proses yang berjalan dengan memanggil fungsi waitpid dengan parameter pidUNZIP, statusUNZIP, dan 0.
- `if (WIFEXITED(statusUNZIP) && !WEXITSTATUS(statusUNZIP)) {` Memeriksa apakah proses yang telah dijalankan berhasil dan menghapus file.zip jika berhasil diekstrak dengan menggunakan perintah rm.
- `pid_t pidRM = fork()` Jika benar, membuat variabel bertipe data pid_t dengan nama pidRM yang berfungsi untuk menyimpan ID proses saat melakukan operasi fork().
- `if (pidRM == 0) {` Memeriksa apakah nilai pidRM sama dengan 0. Jika benar, maka kita berada di dalam child process dan akan menjalankan perintah execlp.
- `execlp("rm", "rm", dir, NULL)` Memanggil fungsi execlp dengan parameter "rm", "rm", dir, NULL. Fungsi ini digunakan untuk menghapus file dengan nama yang sesuai dengan nilai dir.
- `exit(0)` Mengakhiri child process
- `int statusRM` Variabel statusRM akan menyimpan status akhir dari proses yang telah dijalankan.
- `waitpid(pidRM, &statusRM, 0)` Memeriksa status dari proses yang berjalan dengan memanggil fungsi waitpid dengan parameter pidRM, statusRM, dan 0.

```
void removePlayer(char *directory) {
  DIR *folder;
  struct dirent *entry;

  folder = opendir(directory);

  while ((entry = readdir(folder)) != NULL) {
    if (entry->d_type == DT_DIR) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
        continue;

      char subdirectory[strlen(directory) + strlen(entry->d_name) + 2];
      sprintf(subdirectory, "%s/%s", directory, entry->d_name);
      pid_t pidRP = fork();

      if (pidRP == 0) {
        removePlayer(subdirectory);
        exit(EXIT_SUCCESS);
      } else {
        int stat;
        waitpid(pidRP, &stat, 0);
      }
    } else if (entry->d_type == DT_REG) {
      char *extension = strrchr(entry->d_name, '.');
      if (extension && strcmp(extension, ".png") == 0) {
        char *search_result = strstr(entry->d_name, "ManUtd");
        if (search_result == NULL) {
          char file_path[strlen(directory) + strlen(entry->d_name) + 2];
          sprintf(file_path, "%s/%s", directory, entry->d_name);
          remove(file_path);
        }
      }
    }
  }

  closedir(folder);
}
```
- `void removePlayer(char *directory) {` Mendefinisikan fungsi removePlayer dengan parameter directory yang merupakan nama dari direktori yang akan dihapus.
- `DIR *folder` Membuat pointer folder untuk menyimpan alamat direktori yang akan dihapus.
- `struct dirent *entry` Membuat pointer entry untuk menyimpan setiap entri dalam direktori tersebut.
- `folder = opendir(directory)` Membuka direktori yang akan dihapus dan menyimpan alamat direktori tersebut dalam pointer folder.
- `while ((entry = readdir(folder)) != NULL) {` Mengulangi proses membaca setiap entri dalam direktori tersebut hingga tidak ada lagi entri yang tersedia.
- `if (entry->d_type == DT_DIR) {` Memeriksa apakah entri tersebut adalah direktori.
- `if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)` Memeriksa apakah nama direktori adalah "." atau "..", jika ya, maka proses dilanjutkan ke entri selanjutnya.
- `char subdirectory[strlen(directory) + strlen(entry->d_name) + 2]` Mendefinisikan variabel subdirectory yang akan menyimpan alamat subdirektori yang akan dihapus.
- `sprintf(subdirectory, "%s/%s", directory, entry->d_name)` Menyimpan alamat subdirektori ke variabel subdirectory.
- `pid_t pidRP = fork()` Membuat child process baru dengan menggunakan fork() dan menyimpan PID child process tersebut dalam variabel pidRP.
- `if (pidRP == 0) { removePlayer(subdirectory); exit(EXIT_SUCCESS)` Memeriksa apakah child process berhasil dibuat. Jika ya, maka child process akan menjalankan fungsi removePlayer() dengan parameter subdirectory, dan kemudian exit dengan status EXIT_SUCCESS.
- `else { int stat; waitpid(pidRP, &stat, 0); }` Jika child process gagal dibuat, maka parent process akan menunggu child process selesai dan kemudian mengambil status exit dari child process tersebut.
- `else if (entry->d_type == DT_REG)` Memeriksa apakah entri tersebut adalah file regular.
- `char *extension = strrchr(entry->d_name, '.');` Mencari karakter "." terakhir dalam nama file tersebut dan menyimpan pointer ke karakter tersebut dalam variabel extension.
- `if (extension && strcmp(extension, ".png") == 0)` Memeriksa apakah file tersebut memiliki ekstensi ".png".
- `char *search_result = strstr(entry->d_name, "ManUtd")` Mencari substring "ManUtd" dalam nama file tersebut dan menyimpan pointer ke karakter pertama dalam substring tersebut dalam variabel search_result.
- `if (search_result == NULL)` Jika substring "ManUtd" tidak ditemukan dalam nama file tersebut, maka file tersebut akan dihapus.
- `char file_path[strlen(directory) + strlen(entry->d_name) + 2]` membuat array file_path dengan ukuran panjang direktori + panjang nama file + 2 untuk "/" dan karakter nul
- `sprintf(file_path, "%s/%s", directory, entry->d_name)` mengisi array file_path dengan string direktori dan nama file yang saat ini sedang diproses
- `remove(file_path)` menghapus file yang berada pada file_path
- `closedir(folder)` menutup direktori yang sedang dibaca

```
void makeDirectory() {
    pid_t pidMAKEDIR = fork();
    if (pidMAKEDIR == 0) {
        execlp("mkdir", "mkdir", "-p", "players/Kiper", "players/Bek", "players/Gelandang", "players/Penyerang", NULL);
        exit(0);
    }

    int statusMAKEDIR;
    waitpid(pidMAKEDIR, &statusMAKEDIR, 0);
}
```
- `pid_t pidMAKEDIR = fork()` Membuat proses baru dengan fungsi fork(), kemudian mengembalikan nilai proses id (pid) dari proses tersebut ke dalam variabel pidMAKEDIR.
- `if (pidMAKEDIR == 0) {` Dilakukan pengecekan apakah nilai pidMAKEDIR sama dengan 0, yang menandakan bahwa proses yang sedang berjalan saat ini adalah proses child. Jika nilai pidMAKEDIR sama dengan 0, maka akan dijalankan kode pada bagian ini.
- `execlp("mkdir", "mkdir", "-p", "players/Kiper", "players/Bek", "players/Gelandang", "players/Penyerang", NULL);` Mengganti proses saat ini dengan proses baru menggunakan fungsi execlp(). Kode ini akan memanggil perintah mkdir dengan argumen -p dan beberapa nama direktori yang ingin dibuat yaitu players/Kiper, players/Bek, players/Gelandang, dan players/Penyerang.
- `exit(0);` Menghentikan proses saat ini dengan status exit 0.
- `int statusMAKEDIR; waitpid(pidMAKEDIR, &statusMAKEDIR, 0)` Menunggu proses child selesai dieksekusi dengan fungsi waitpid(). Status dari proses child akan disimpan pada variabel statusMAKEDIR.

```
void categorizePlayer(char *directory) {

  DIR *folder;
  struct dirent *entry;

  folder = opendir(directory);

  while ((entry = readdir(folder)) != NULL) {
    if (entry->d_type == DT_REG) {
      char *extension = strrchr(entry->d_name, '.');
      if (extension && strcmp(extension, ".png") == 0) {
        char *search_result = strstr(entry->d_name, "ManUtd");
        if (search_result != NULL) {
          char file_path[strlen(directory) + strlen(entry->d_name) + 2];
          sprintf(file_path, "%s/%s", directory, entry->d_name);

          char *posisi = NULL;

          // Cek posisi pemain berdasarkan nama file
          if (strstr(entry->d_name, "Kiper")) {
            posisi = "Kiper";
          } else if (strstr(entry->d_name, "Bek")) {
            posisi = "Bek";
          } else if (strstr(entry->d_name, "Gelandang")) {
            posisi = "Gelandang";
          } else if (strstr(entry->d_name, "Penyerang")) {
            posisi = "Penyerang";
          }

          // Pindahkan file ke folder yang sesuai
          if (posisi != NULL) {
            char new_path[strlen(posisi) + strlen(entry->d_name) + strlen(directory) + 3];
            sprintf(new_path, "%s/%s/%s", directory, posisi, entry->d_name);

            pid_t pidMV = fork();
            if (pidMV == 0) {
              execlp("mv", "mv", file_path, new_path, NULL);
              exit(0);
            } else {
              int statusMV;
              waitpid(pidMV, &statusMV, 0);
            }
          }
        }
      }
    }
  }

  closedir(folder);
}
```
Penjelasan Kode :
```
void categorizePlayer(char *directory) {

  DIR *folder;
  struct dirent *entry;

  folder = opendir(directory);
void categorizePlayer(char *directory) {

  DIR *folder;
  struct dirent *entry;

  folder = opendir(directory);
```
- `void categorizePlayer(char *directory)` adalah deklarasi dari fungsi `categorizePlayer` yang menerima sebuah string sebagai parameter.
- `DIR *folder` dan `struct dirent *entry` adalah variabel yang dideklarasikan sebagai pointer ke `DIR` dan `dirent` structs, masing-masing. DIR struct digunakan untuk merepresentasikan sebuah directory stream, sementara dirent struct digunakan untuk merepresentasikan sebuah directory entry.
- `folder = opendir(directory)` digunakan untuk membuka sebuah directory yang diwakili oleh directory. opendir adalah fungsi bawaan dari sistem operasi untuk membuka directory stream dan mengembalikan pointer ke DIR struct. Jika directory stream berhasil dibuka, pointer akan disimpan di dalam variabel folder.
- `while ((entry = readdir(folder)) != NULL)` digunakan untuk melakukan iterasi setiap entry dalam directory stream yang telah dibuka. readdir adalah fungsi bawaan sistem operasi untuk membaca directory stream. Setiap entry dari directory stream akan ditempatkan ke dalam variabel entry dan selama entry masih ada, loop akan terus berjalan.
- `if (entry->d_type == DT_REG)` digunakan untuk mengecek apakah tipe dari entry saat ini adalah regular file. `entry->d_type` adalah salah satu member dari dirent struct yang merepresentasikan tipe dari directory entry.
- `char *extension = strrchr(entry->d_name, '.')` digunakan untuk mencari dan menyimpan extension file dari entry di dalam variabel extension. `strrchr` adalah fungsi bawaan sistem operasi untuk mencari karakter terakhir yang cocok dari sebuah string.
- `if (extension && strcmp(extension, ".png") == 0)` digunakan untuk memastikan apakah extension dari file adalah `.png`.
- `char *search_result = strstr(entry->d_name, "ManUtd")` digunakan untuk mencari apakah nama file mengandung string "ManUtd". strstr adalah fungsi bawaan sistem operasi untuk mencari substring dari sebuah string.
- `if (search_result != NULL)` digunakan untuk memastikan bahwa nama file mengandung substring "ManUtd".
- `char file_path[strlen(directory) + strlen(entry->d_name) + 2]` digunakan untuk menyimpan path lengkap dari file dengan menambahkan directory dan `entry->d_name`. strlen adalah fungsi bawaan sistem operasi untuk mengembalikan panjang sebuah string.
- `char *posisi = NULL` adalah deklarasi variabel posisi dengan inisialisasi NULL. Variabel posisi digunakan untuk menentukan posisi pemain yang terdapat dalam file.

```
// Cek posisi pemain berdasarkan nama file
if (strstr(entry->d_name, "Kiper")) {
    posisi = "Kiper";
} else if (strstr(entry->d_name, "Bek")) {
    posisi = "Bek";
} else if (strstr(entry->d_name, "Gelandang")) {
    posisi = "Gelandang";
} else if (strstr(entry->d_name, "Penyerang")) {
    posisi = "Penyerang";
}
```
- Baris pertama ini menggunakan fungsi strstr() untuk mencari string "Kiper", "Bek", "Gelandang", atau "Penyerang" di dalam nama file yang dijadikan input. Fungsi strstr() akan mengembalikan pointer ke posisi pertama dari substring yang dicari di dalam string entry->d_name. Jika substring tersebut tidak ditemukan, maka akan mengembalikan nilai NULL.
- Jika substring "Kiper" ditemukan di dalam nama file, maka variabel posisi akan diisi dengan string "Kiper". Jika substring "Kiper" tidak ditemukan, maka akan dilanjutkan ke baris berikutnya untuk mencari substring "Bek". Proses ini akan dilakukan secara berurutan sampai sebuah substring ditemukan atau tidak ada lagi substring yang tersisa.

```
// Pindahkan file ke folder yang sesuai
if (posisi != NULL) {
    char new_path[strlen(posisi) + strlen(entry->d_name) + strlen(directory) + 3];
    sprintf(new_path, "%s/%s/%s", directory, posisi, entry->d_name);

    pid_t pidMV = fork();
    if (pidMV == 0) {
        execlp("mv", "mv", file_path, new_path, NULL);
        exit(0);
    } else {
        int statusMV;
        waitpid(pidMV, &statusMV, 0);
    }
}
```
- `if (posisi != NULL) {` Baris pertama ini memeriksa apakah variabel posisi memiliki nilai yang tidak NULL. Jika iya, maka akan dilakukan proses pemindahan file ke dalam folder yang sesuai. Jika tidak, maka proses pemindahan file tidak akan dilakukan.
- `char new_path[strlen(posisi) + strlen(entry->d_name) + strlen(directory) + 3]` Baris kedua ini membuat sebuah array karakter new_path dengan ukuran yang cukup besar untuk menampung path direktori tujuan file yang akan dipindahkan. Panjang array ini dihitung dengan menjumlahkan panjang string posisi, string entry->d_name, dan string directory, ditambah 3 karakter (2 karakter "/" dan 1 karakter null terminator).
- `sprintf(new_path, "%s/%s/%s", directory, posisi, entry->d_name);` Baris ketiga ini menggunakan fungsi sprintf() untuk memformat path direktori tujuan file yang akan dipindahkan ke dalam array karakter new_path. Path direktori tujuan dibuat dengan format "%s/%s/%s", dengan nilai variabel directory sebagai direktori utama, nilai variabel posisi sebagai sub-direktori yang sesuai dengan posisi pemain, dan nilai variabel entry->d_name sebagai nama file yang akan dipindahkan.

```
pid_t pidMV = fork();
if (pidMV == 0) {
  execlp("mv", "mv", file_path, new_path, NULL);
  exit(0);
} else {
  int statusMV;
  waitpid(pidMV, &statusMV, 0);
}
```
Baris keempat hingga keenam ini melakukan pemindahan file dengan menggunakan perintah mv melalui proses fork(). Proses anak akan menjalankan perintah mv menggunakan fungsi execlp(), dengan argumen-argumen "mv", file_path, dan new_path. Proses anak kemudian akan diakhiri dengan exit(0). Proses induk akan menunggu proses anak selesai dengan menggunakan fungsi waitpid(), dan kemudian memproses file berikutnya.
- `closedir(folder);` Baris terakhir ini menutup direktori yang telah dibuka pada awal program.

```

void makeTeam(int bek, int gelandang, int penyerang) {
    char* posisi[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};
    int jumlah_pemain[] = {1, bek, gelandang, penyerang};


    // Membuka file output di direktori players
    char output_file_path[100];
    sprintf(output_file_path, "players/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *output_file = fopen(output_file_path, "w");

    // Menulis header file output
    fprintf(output_file, "Formasi %d-%d-%d\n", bek, gelandang, penyerang);
    fprintf(output_file, "-----------------\n");

    // Mencari file pemain di setiap posisi
    for (int i = 0; i < 4; i++) {
        DIR *folder;
        struct dirent *entry;

        char directory[100];
        sprintf(directory, "players/%s", posisi[i]);

        folder = opendir(directory);

        // Mencari pemain dengan rating tertinggi di setiap posisi
        int count = 0;
        while ((entry = readdir(folder)) != NULL && count < jumlah_pemain[i]) {
            if (entry->d_type == DT_REG) {
                char file_path[strlen(directory) + strlen(entry->d_name) + 2];
                sprintf(file_path, "%s/%s", directory, entry->d_name);

                // Membaca rating pemain dari nama file
                int rating = atoi(strtok(entry->d_name, "_"));

                // Menuliskan informasi pemain ke file output
                fprintf(output_file, "%s %d %s\n", posisi[i], rating, entry->d_name);

                count++;
            }
        }

        closedir(folder);
    }

    fclose(output_file);
}
```
- `char* posisi[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};` Variabel ini adalah array dari string yang berisi nama posisi pemain sepak bola yang akan dicari, yaitu Kiper, Bek, Gelandang, dan Penyerang.
- `int jumlah_pemain[] = {1, bek, gelandang, penyerang};` Variabel ini adalah array dari integer yang menyimpan jumlah pemain yang dibutuhkan untuk setiap posisi yang sesuai dengan formasi yang dipilih. Jumlah pemain di posisi kiper selalu satu, sementara posisi lainnya tergantung pada nilai input yang diberikan pada saat memanggil fungsi makeTeam.
- `char output_file_path[100];` Variabel ini adalah string yang berisi path file output yang akan digunakan untuk menuliskan informasi tim yang terbentuk.
- `sprintf(output_file_path, "players/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);` Fungsi ini digunakan untuk mengisi nilai pada output_file_path sesuai dengan formasi yang dipilih.
- `FILE *output_file = fopen(output_file_path, "w");` Membuka file output dengan path yang telah dibuat sebelumnya dan mode write ("w").
- `fprintf(output_file, "Formasi %d-%d-%d\n", bek, gelandang, penyerang)` Menulis informasi formasi yang dipilih pada file output.
- `fprintf(output_file, "-----------------\n");` Menulis separator pada file output.
- `for (int i = 0; i < 4; i++) {` Loop untuk setiap posisi pemain yang akan dicari.
- `DIR *folder;` Variabel ini merupakan tipe data khusus untuk menyimpan direktori yang akan dibuka.
- `struct dirent *entry;` Variabel ini digunakan untuk menyimpan nama file yang ditemukan di dalam direktori.
- `sprintf(directory, "players/%s", posisi[i]);` Mengisi variabel directory dengan path direktori pemain pada posisi yang sedang dicari.
- `folder = opendir(directory);` Membuka direktori yang sesuai dengan path yang diberikan.
- `while ((entry = readdir(folder)) != NULL && count < jumlah_pemain[i]) {` Loop untuk mencari pemain dengan rating tertinggi pada posisi yang sedang dicari. Loop akan berhenti jika sudah menemukan jumlah pemain yang dibutuhkan pada posisi tersebut.
- `if (entry->d_type == DT_REG) {` Mengecek apakah nama file yang ditemukan merupakan file regular (bukan direktori atau file khusus lainnya).
- `int rating = atoi(strtok(entry->d_name, "_"));` Membaca rating pemain dari nama file dengan memisahkan nama file menggunakan _ dan mengubah rating yang ditemukan menjadi integer.
- `fprintf(output_file, "%s %d %s\n", posisi[i], rating, entry->d_name);` Menulis informasi pemain ke file output. Informasi yang ditulis berupa nama posisi pemain, rating pemain, dan nama pemain. %s digunakan untuk menampilkan string, %d digunakan untuk menampilkan bilangan bulat (integer), dan \n digunakan untuk membuat baris baru. Variabel `posisi[i]` digunakan untuk menampilkan posisi pemain, variabel rating digunakan untuk menampilkan rating pemain, dan variabel `entry->d_name` digunakan untuk menampilkan nama pemain.
- `count++;:` Menambah nilai variabel count dengan 1 setiap kali sebuah pemain berhasil dituliskan ke file output.
- `closedir(folder);:` Menutup direktori yang sedang dibuka setelah selesai mencari pemain di posisi tersebut.
- `fclose(output_file);:` Menutup file output setelah selesai menuliskan informasi pemain ke dalamnya.

```
int main() {
  downloadURL("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF");
  unzipDirectory("players.zip");
  removePlayer("players");
  makeDirectory();
  categorizePlayer("players");
  makeTeam(4, 3, 3);
  return 0;
}
```
- `downloadURL("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF");` Memanggil fungsi downloadURL untuk mendownload file dari URL tertentu.
- `unzipDirectory("players.zip");` Memanggil fungsi unzipDirectory untuk mengekstrak file zip "players.zip" yang telah didownload sebelumnya.
- `removePlayer("players");` Memanggil fungsi removePlayer untuk menghapus folder "players" jika folder tersebut sudah ada sebelumnya.
- `makeDirectory();` Memanggil fungsi makeDirectory untuk membuat folder "players" di direktori saat ini.
- `categorizePlayer("players");` Memanggil fungsi categorizePlayer untuk memindahkan file-file pemain ke folder yang sesuai dengan posisi mereka.
- `makeTeam(4, 3, 3);` Memanggil fungsi makeTeam untuk membuat tim berdasarkan formasi 4-3-3.

## Soal 4
4. Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
    - Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
    - Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
    - Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
    - Bonus poin apabila CPU state minimum.

Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

## Penyelesaian
Pada soal ini kita diminta untuk membuat program c yang bekerja seperti crontab. pada soal kita juga diminta untuk tidak menggunakan system dan program harus berjalan secara daemon.

libaray yang kami gunakan dalam pembuatan program ini yaitu
```Ruby
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

```
pada soal diberikan contoh untuk melakukan run program seperti `./program \* 44 5 /home/Banabil/programcron.sh` lalu di lakukan pengecekan argumen dengan syntax berikut

```Ruby
 int hour, minute, second;
  char *path;
  char cmd[1000];
  // Parse command-line arguments
  if (argc != 5) {
      fprintf(stderr, "Error: Invalid number of arguments.\n");
      exit(1);
  }
  if (strcmp(argv[1], "*") == 0) {
      hour = -1;
  } else {
      hour = atoi(argv[1]);
      if (hour < 0 || hour > 23) {
          fprintf(stderr, "Error: Invalid hour argument.\n");
          exit(1);
      }
  }
  if (strcmp(argv[2], "*") == 0) {
      minute = -1;
  } else {
      minute = atoi(argv[2]);
      if (minute < 0 || minute > 59) {
          fprintf(stderr, "Error: Invalid minute argument.\n");
          exit(1);
      }
  }
  if (strcmp(argv[3], "*") == 0) {
      second = -1;
  } else {
      second = atoi(argv[3]);
      if (second < 0 || second > 59) {
          fprintf(stderr, "Error: Invalid second argument.\n");
          exit(1);
      }
  }
  path = argv[4];

```
- pada `if (argc != 5)` dilakukan pengecekan apakah argumen yang diberikan sudah cukup atau belum
- pada percabangan kedua dilakukan pengecekan argumen jam yang diberikan. jika argumen yang diberikan yaitu `*` maka `hour = -1;` jika argumen yang diberikan lebih dari 24 maka akan error
- Pada percabangan ketiga dilakukan pengecekan untuk argumen menit yang dimasukan. sama seperti percabangan sebelumnya, jika argumen yang diberikan `*` maka `minute= -1;`. jika argumen yang diberikan lebih dari 59 maka akan error. karenan dalam 1 jam hanya ada 60 menit.
- pada percabangan keempat dilakukan pengecekan untuk argumen detik. sama juga seperti percabangan sebelumnya, jika argumen yang diberikan `*` maka nilai dari `second = -;`jika argumen yang diberikan lebih dari 59 maka akan error, karena dalam 1 menit hanya terdapat 60 detik.
- selanjutnya untuk argumen keempat yaitu merupakan path dari file yang akan dilakukan cron. maka path tersebut ditambahkan pada variabel path.

setelah dilakukan pengecekan untuk semua argumen dilakukan daemon dengan menggunakan template daemon yang sudah ada

```Ruby
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/syukra/sisop/Modul2/soal4/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
}

```
di dalam perulangan dilakukan pengecekan kesusaian jam saat ini dengan jam yang ada pada argumen. berikut syntax yang digunakan

```Ruby
    pid_t id,id1;
    id=fork();
    if(id<0)exit(EXIT_FAILURE);
    if(id==0){  
    
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    int curr_hour = tm->tm_hour;
    int curr_minute = tm->tm_min;
    int curr_second = tm->tm_sec;

    if ((hour == -1 || hour == curr_hour) &&
        (minute == -1 || minute == curr_minute) &&
        (second == -1 || second == curr_second)) {
        char *cmd[] = {"bash",path};
        execv("/bin/bash",cmd);
      }
    exit(0);
    }
    sleep(1);
  }

```
- pada syntax tersebut kita menggunakan fork karena akan menggunakan `execv()`. jika tidak menggunakan `fork()` maka prosesnya akan tergantikan begitu saja dan program selesai begitu saja. 
- pada percabangan
`if ((hour == -1 || hour == curr_hour) &&
        (minute == -1 || minute == curr_minute) &&
        (second == -1 || second == curr_second))` dilakukan pengecekan apakah waktu saat itu sama dengan variabel yang diberikan. jika sama maka akan dilakukan `char *cmd[] = {"bash",path};
        execv("/bin/bash",cmd);` yang mana yaitu perintah untuk bash suatu file pada path yang sudah kita masukan sebelumnya.
- `sleep(1)`berfungsi untuk jeda program selama 1 detik saja, jadi pengecekan waktu dilakukan setiap detik.
