#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>

int main(int argc, char ** argv) {
	
  FILE * killer = fopen("killer.sh", "w");
	fprintf(killer, "#!/bin/bash\n");

  if(strcmp("-a", argv[1]) == 0){
		fprintf(killer, "killall -SIGKILL lukisan\n");//untuk kill semua process dengan nama test pada saat itu juga
	}else if(strcmp("-b", argv[1]) == 0){
		fprintf(killer,"kill -9 %d\n",(int)getpid()+1);//untuk kill parent process saja sehingga child masih dapat menyelesaikan procesnya
	}
	fprintf(killer, "rm $0\n");
	fclose(killer);

	chmod("killer.sh", ~0);


  pid_t pid, sid;        // Variabel untuk menyimpan PID
  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  char path[1024];
  char bas[1024]; 
  getcwd(path,sizeof(path));//mendapatkan direktori kerja saat ini
  getcwd(bas,sizeof(bas));
  
  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir(path)) < 0) {
    exit(EXIT_FAILURE);
  }
  
  strcat(path,"/");

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
  
  while (1) {
      pid_t pid1, id1;
      pid1 =fork();
        if(pid1 < 0)exit(0);
        if(pid1 == 0){ 
          char dirname[20]; // untuk menampung nama direktori
          time_t t = time(NULL); // mendapatkan waktu saat ini
          struct tm tm = *localtime(&t);
          strftime(dirname, sizeof(dirname), "%Y-%m-%d_%H:%M:%S", &tm);//menyimpan waktu pada saat itu ke dirname
          pid_t id =fork();
          if(id <0)exit(0);
          if(id==0){
                char *arg[] = {"mkdir","-p",dirname,NULL};//menyimpan argumen untuk membuat dirktori baru
                execv("/bin/mkdir", arg);}
          waitpid(id,NULL,0);
          strcat(path,dirname);
          for(int i = 0; i <=15; i++){
          id1 = fork();
          if(id1 <0)exit(0);
          if(id1 == 0){
              if(chdir(path) < 0)exit(EXIT_FAILURE);
              char filename[1024];
              char url[1024];
              t = time(NULL);
              tm = *localtime(&t);
              strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S", &tm);
              snprintf(url,sizeof(url), "https://picsum.photos/%ld", t%1000+50);
              char * args[] = {"wget",url,"-O",filename,"-o","/dev/null",NULL}; // argumen untuk download foto
              execv("/usr/bin/wget", args);
              }
            sleep(5);
            }// jeda download foto 5 detik
        if(chdir(bas) < 0)exit(EXIT_FAILURE);
        waitpid(id1,NULL,0);//berfungsi untuk menunggu semua download seleseai terlebih dahulu baru zipping dijalankan
        //zipping
				char fileName[30];
        strcat(fileName,dirname);
				snprintf(fileName, sizeof(fileName), "%s.zip", dirname);
				char * arg[] = {"zip", "-rm", fileName, dirname, NULL};
				execv("/usr/bin/zip", arg);
        }
    sleep(30);//jeda pembuat direktori 30 detik
  }
}
